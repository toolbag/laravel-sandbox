<?php

namespace Services\Animal;

/*
Example of Service class where some retrieval business logic is encapsulate, so
this doesn't have to be sprinkled around Controllers.

For example, you might imagine that Constructor takes as input a User object which is a "context"
and we can scope queries to the User or whatever, so users can only access their own posts, etc.
*/

class AnimalFinder {

    private $user;
    
    public function __construct($user = null) {
        $this->user = $user;
    }
    
    public function find($id) {
        return \Animal::find($id);
    }
    
    /*
        $options +array+: 
            'initial' +String+ validated to be in the range a-z
    */
    public function all(array $options = null) {
        if(is_array($options) && array_key_exists('initial', $options)) {
            $initial = strtolower($options['initial']);
            if(preg_match('/[a-z]{1}/', $initial)) {
                return \Animal::where('initial', '=', $initial)->get();
            } else {
                App::abort(500, 'Initial is invalid - must satisfy [a-z]');
            }
        } else {
            return \Animal::all();
        }
    }
}
?>