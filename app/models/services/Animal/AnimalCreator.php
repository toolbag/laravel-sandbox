<?php

namespace Services\Animal;

/*
Example of Service class where some retrieval business logic is encapsulate, so
this doesn't have to be sprinkled around Controllers.

For example, you might imagine that Constructor takes as input a User object which is a "context"
and we can scope queries to the User or whatever, so users can only access their own posts, etc.
*/

class AnimalCreator {

    private $user;
    
    public function __construct($user = null) {
        $this->user = $user;
    }
    
    public function create(Array $params) {
        $animal = new \Animal($params);
        $animal->save();
        
        return $animal;
    }
    
    public function update(\Animal $animal, Array $params) {
        // TODO: how can we mass assign attributes?
        $animal->name = $params['name'];
        return $animal->save();
    }
    
}
?>