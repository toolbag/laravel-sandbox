<?php

namespace Services\Animal;

class AnimalValidator {

    private $user;
    private $validator = null;
    
    public function __construct($user = null) {
        $this->user = $user;
        
    }
    
    public function validate(Array $animal_data) {
        $this->validator = \Validator::make(
            $animal_data,
            array('name' => 'required|min:3')
        );
        
        return $this->passes();
    }
    
    public function passes() {
        return $this->validator->passes();
    }

    public function fails() {
        return $this->validator->fails();
    }
    
    public function messages() {
        return $this->validator->messages();
    }
}
?>