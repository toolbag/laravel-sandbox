<?php

class Animal extends Eloquent {
	
	protected $table = 'animals';
	protected $fillable = array('name');

    public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $this->attributes['initial'] = strtolower(substr($value, 0, 1));
    }

    /*
    public static function boot()
    {
        parent::boot();

        static::creating(function($animal)
        {
            $animal->set_initial();
        });

        static::updating(function($animal)
        {
            $animal->set_initial();
        });
        
    }
    */
    
    /* Lame that this has to be a *public+ method. It should be +private+. However Eloquent cannot
    see it if its non public, in the +creating+ and +updating+ callbacks */
    /*
    public function set_initial() {
        if(!empty($this->name)) {
            $this->attributes['initial'] = strtolower(substr($this->attributes['name'], 0, 1));
        }
    }
    */

}