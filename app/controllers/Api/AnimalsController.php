<?php

namespace Controllers\Api;

use App\Models\Animal;
use App\Models\Services\AnimalFinder;
use Illuminate\Routing\Controllers\Controller;

class AnimalsController extends \BaseController {

	public function index()
	{
	    $options = array();

	    if(\Input::has('initial')) {
            $options['initial'] = \Input::get('initial');
        }

	    $animals = $this->animal_finder()->all($options);
	    return \Response::json($animals);
	}

    public function show($id) {
        return \Response::json($this->animal_finder()->find($id));
    }

    public function destroy($id) {
	    $animal = $this->animal_finder()->find($id);
	    $animal->delete();
        return \Response::json(array('status' => 'ok'));
    }
    

    /*
        Expects Request Body to be JSON formatted like:
        {
            "animal": {
                "name": "THE NEW ANIMAL NAME"
            }
        }
    */
    public function update($id) {
        $animal = $this->animal_finder()->find($id);
        
	    $animal_validator = new \Services\Animal\AnimalValidator();
        
        $params = \Input::json();
        if($params->has('animal')) {
            $animal_params = $params->get('animal');
            if($animal_validator->validate($animal_params)) {
                $creator = new \Services\Animal\AnimalCreator();
                if($animal = $creator->update($animal, $animal_params)) {
                    return \Response::json(array('status' => 'ok'));
                }
            } else {
                $errors = $animal_validator->messages();
                return \Response::json(array('status' => 'error', 'errors' => $errors->all()), 500);
            }
        }
    }
}