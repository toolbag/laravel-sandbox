<?php

class AnimalsController extends \BaseController {

	public function index()
	{
	    $options = array();

	    if(Input::has('initial')) {
            $options['initial'] = Input::get('initial');
        }
	    $data = array('animals' => $this->animal_finder()->all($options));
		return View::make('animals.index', $data);
	}

	public function show($id) {
	    $animal = $this->animal_finder()->find($id);
		return View::make('animals.show', array('animal' => $animal));
    }
	
	public function edit($id) {
	    $animal = $this->animal_finder()->find($id);
		return View::make('animals.edit', array('animal' => $animal));
    }
    
    public function create() {
        $animal = new \Animal();
		return View::make('animals.create', array('animal' => $animal));
    }
    
    public function destroy($id) {
	    $animal = $this->animal_finder()->find($id);
	    $animal->delete();
	    return Redirect::action('AnimalsController@index');
    }

    /*
     TODO: refactor +create+ and +store+ to de-duplicate common functionality
     of validating and creating/updating models.
     
     Maybe we can use closures with +success+ and +failure+ callbacks ???
    */
    public function update($id) {
	    $animal = $this->animal_finder()->find($id);
	    
	    $animal_validator = new \Services\Animal\AnimalValidator();
        
        $params = Input::all();
        if($animal_validator->validate($params)) {
            $creator = new \Services\Animal\AnimalCreator();
            if($animal = $creator->update($animal, $params)) {
        	    return Redirect::action('AnimalsController@index');
            }
        } else {
            $errors = $animal_validator->messages();
    		return View::make('animals.create', array('animal' => $animal, 'errors' => $errors));
        }
    }
    
    public function store() {
        $params = Input::all();
        
        $animal_validator = new \Services\Animal\AnimalValidator();
        
        if($animal_validator->validate($params)) {
            $creator = new \Services\Animal\AnimalCreator();
            if($animal = $creator->create($params)) {
        	    return Redirect::action('AnimalsController@index');
            }
        } else {
            $errors = $animal_validator->messages();
            $animal = new \Animal($params);
    		return View::make('animals.create', array('animal' => $animal, 'errors' => $errors));
        }
    }

}