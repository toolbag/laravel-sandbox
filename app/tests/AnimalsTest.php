<?php

class AnimalsTest extends TestCase {

    private function prepareForTests()
    {
        Artisan::call('migrate');
        Mail::pretend(true);
    }
    
    public function setUp() {
        parent::setUp(); 
        $this->prepareForTests();
        
	    // Create some test Animals
        $names = array('Aardvark', 'Elephant', 'Monkey', 'Tiger', 'Newt', 'Eagle');
        foreach($names AS $name) {
            Animal::create(array('name' => $name));
	    }
    }
    
    public function tearDown() {
        parent::tearDown();
        
        // clean up after ourselves
        DB::statement('DELETE FROM animals');
    }
    
    public function test_creating_an_animal_sets_its_initial() {
        $name = "Parrot";
        $animal = new Animal();
        $animal->name = $name;
        $animal->save();
        $this->assertEquals("p", $animal->initial);
    }
    
    // GET /animals
	public function test_animals_index()
	{
		$response = $this->call('GET', '/animals');
		$this->assertResponseOk();
		
		$view = $response->original;
		
		// we created 6 animals so they should all be in the view
		$this->assertCount(6, $view['animals']);
	}

    // GET /animals?initial
	public function test_animals_index_with_initial()
	{
		$response = $this->call('GET', '/animals', array('initial' => 'M'));
		$this->assertResponseOk();
		
		$view = $response->original;
		$this->assertCount(1, $view['animals']);
	}
	
	// GET /animals/:id
	public function test_animal_show() {
	    $animal = Animal::first();
		$response = $this->call('GET', '/animals/' . $animal->id);
		$this->assertResponseOk();
		
		$view = $response->original;
		$assigned_animal = $view['animal'];
		
		$this->assertEquals($animal->name, $assigned_animal->name);
    }
    
    // PUT /animals/:id
    public function test_animal_update() {
	    $animal = Animal::first();
	    $new_name = "My New Animal Name";
	    $data = array('name' => $new_name);
		$response = $this->call('PUT', '/animals/' . $animal->id, $data);
		$this->assertRedirectedTo('/animals');
		
		// verify the animal name was updated
		$animal = Animal::find($animal->id);
		$this->assertEquals($new_name, $animal->name);
    }
    
    public function test_animal_edit() {
	    $animal = Animal::first();
		$response = $this->call('GET', '/animals/' . $animal->id . '/edit');
		$this->assertResponseOk();
    }
    
    public function test_animal_destroy() {
	    $animal = Animal::first();
	    $id = $animal->id;
		$response = $this->call('DELETE', '/animals/' . $animal->id);
        $this->assertRedirectedToAction('AnimalsController@index');
        
        // ensure its not in DB
        $this->assertEquals(NULL, Animal::find($id));
    }

}