<?php

class ApiAnimalsTest extends TestCase {

    private function prepareForTests()
    {
        Artisan::call('migrate');
        Mail::pretend(true);
    }
    
    public function setUp() {
        parent::setUp(); 
        $this->prepareForTests();
        
	    // Create some test Animals
        $animals = array('Aardvark', 'Elephant', 'Monkey', 'Tiger', 'Newt', 'Eagle');
        foreach($animals AS $name) {
            Animal::create(array('name' => $name));
	    }
    }
    
    public function tearDown() {
        parent::tearDown();
        
        // clean up after ourselves
        DB::statement('DELETE FROM animals');
    }

    // GET /api/animals
	public function test_api_animals_index()
	{
		$response = $this->call('GET', '/api/animals');
		$this->assertResponseOk();
		
		$animals = json_decode($response->getContent());
		// we created 6 animals so they should all be in the view
		$this->assertCount(6, $animals);
	}
	
	// GET /animals/:id
	public function test_api_animal_show() {
	    $animal = Animal::first();
		$response = $this->call('GET', '/api/animals/' . $animal->id);
		$this->assertResponseOk();
		
		$json_animal = json_decode($response->getContent());
		
		$this->assertEquals($json_animal->name, $animal->name);
    }
    
    // GET /animals?initial
	public function test_api_animals_index_with_initial()
	{
		$response = $this->call('GET', '/api/animals', array('initial' => 'M'));
		$this->assertResponseOk();
		
		$json_animals = json_decode($response->getContent());
		$this->assertCount(1, $json_animals);
	}
    
    
    // PUT /animals/:id
    public function test_api_animal_update() {
	    $animal = Animal::first();
	    $new_name = "My New Animal Name";
	    $data = array('animal' => array('name' => $new_name));
	    
	    $headers = array('Content-Type' => 'application/json');
		$response = $this->call('PUT', '/api/animals/' . $animal->id, array(), array(), $headers, json_encode($data));
		$json = json_decode($response->getContent(), true);
		$this->assertEquals(array('status' => 'ok'), $json);
		
		// verify the animal name was updated
        $animal = Animal::find($animal->id);
        $this->assertEquals($new_name, $animal->name);
    }
    
    // PUT /animals/:id with error
    public function test_api_animal_update_with_validation_errors() {
	    $animal = Animal::first();
	    // name is tooooo short
	    $data = array('animal' => array('name' => 't'));
	    
	    $headers = array('Content-Type' => 'application/json');
		$response = $this->call('PUT', '/api/animals/' . $animal->id, array(), array(), $headers, json_encode($data));
		$json = json_decode($response->getContent(), true);
		$status = $json['status'];
		$this->assertEquals('error', $status);
		$this->assertEquals(true, array_key_exists('errors', $json));
		$errors = $json['errors'];
		$this->assertEquals(500, $response->getStatusCode());
    }

    public function test_api_animal_destroy() {
	    $animal = Animal::first();
	    $id = $animal->id;
		$response = $this->call('DELETE', '/api/animals/' . $animal->id);
		$this->assertResponseOk();
        
        // ensure its not in DB
        $this->assertEquals(NULL, Animal::find($id));
    }
    
    
}