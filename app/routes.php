<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function()
{
	return Redirect::to('animals');
});

Route::resource('animals', 'AnimalsController');

/* API prefix / namespace */
Route::group(array('prefix' => 'api'), function() {
    Route::resource('animals', 'Controllers\\Api\\AnimalsController');
});
