<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $animals = array('Aardvark', 'Elephant', 'Monkey', 'Tiger', 'Newt', 'Eagle');
        foreach($animals AS $name) {
            $animal = new Animal();
            $animal->name = $name;
            $animal->save();
	    }
	}

}