<?php
foreach ($errors->all() as $error)
{
    echo "<h3>" . htmlentities($error) . "</h3>";
}
?>
<hr />
<?php echo Form::model($animal, array('method' => 'post', 'route' => array('animals.store'))); ?>
    <fieldset>
        <legend>Create an Animal</legend>
        <table>
            <tr>
                <td>Name:</td>
                <td><?php echo Form::text('name'); ?></td>
            </tr>
        </table>
        <input type="submit" name="commit" value="Save" />
    </fieldset>
</form>