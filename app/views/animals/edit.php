<?php echo Form::model($animal, array('method' => 'put', 'route' => array('animals.update', $animal->id))); ?>
    <fieldset>
        <legend>Update your Animal</legend>
        <table>
            <tr>
                <td>Name:</td>
                <td><?php echo Form::text('name'); ?></td>
            </tr>
        </table>
        <input type="submit" name="commit" value="Save" />
    </fieldset>
</form>