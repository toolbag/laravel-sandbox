<h1><?php echo htmlentities($animal->name); ?></h1>
<hr />
<table>
    <tr><th>Initial</th><th>Created</th><th>Updated</th></tr>
    <tr>
        <td><?php echo htmlentities($animal->id); ?></td>
        <td><?php echo $animal->created_at; ?></td>
        <td><?php echo $animal->updated_at; ?></td>
    </tr>
</table>
<hr />
<a href="<?php echo URL::action('AnimalsController@edit', $animal->id); ?>">Edit</a>
<hr />
<?php echo Form::model($animal, array('method' => 'delete', 'route' => array('animals.destroy', $animal->id))); ?>
    <input type="submit" name="commit" value="Delete" />
</form>
