<?php $url = URL::to('animals/create'); ?>
<a href="<?php echo $url; ?>">Create Animal</a>
<hr />
<ul>
    <?php foreach($animals AS $animal): ?>
        <li>
            <?php $url = URL::to('animals', $animal->id); ?>
            <a href="<?php echo $url; ?>"><?php echo htmlentities($animal->name); ?></a>
        </li>
    <?php endforeach; ?>
</ul>