## Setup Development Environment ##

* Create Development Database (sqlite)

```
$ touch app/database/development.sqlite
```

* Run Migrations

```
$ php artisan migrate
```

* Seed DB with sample data

```
$ php artisan db:seed
```

You can now access the PHP app in your browser and should be able to view a list of animals and edit existing ones.

## API ##

The same web actions are accessible via a JSON based API, which uses the `/api/` prefix

Try it out on the command line with CURL:

View All Animals:

```
$ curl -H 'Content-Type: application/json' http://laravel.dev:5555/api/animals
```

Show a single Animal:

```
$ curl -H 'Content-Type: application/json' http://laravel.dev:5555/api/animals/2
```

Update an Animals name:

```
$ curl -XPUT -d '{"animal":{"name":"New Elephant Name"}}' -H 'Content-Type: application/json' http://laravel.dev:5555/api/animals/2
```

## AnimalFinder ##

The Controllers need to fetch Animals. In a trivial example app you can embed retrieval methods directly in Controllers, especially if you're fetching a "public" object from the DB. But often-times you need to have more logic in a retrieval - "fetch a Post given a specific User & Post ID". You might be doing this all over the application and you don't want to duplicate a multi-line DB query in every controller.

In this case off-loading that logic to a single class is the sanest route. An example is `AnimalFinder` which is used in both Controllers and centralizes all retrieval logic.

Eventually you'd want to move all Animal business logic read / write to this class, so you'd want to use it for writes as well. In which case you'd want to change the name to something like `AnimalService` or whatever...

## Testing ##

A complete test suite for web & API operations are available in `app/tests`. Run the suite via:

```
$ ENV=testing vendor/bin/phpunit
```